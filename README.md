**Requirements**: **Java 8 and above.**

Tested only on windows 64-bit and ubuntu 15.10/amd64.


**Download it from** [HERE](https://bitbucket.org/pyrexed/ms/downloads/MS-0.4.1.jar)

**Usage**:

Double click the JAR file, enter your username and password and press start.

In the Course selection view, select the courses you want to download. You can select the root path where all folders will be created either by editing the text manually or using the right mouse button on the text-field to launch a file/directory chooser.


Please report any problems.

If a new version is giving you problems, try one of the older versions [over here](https://bitbucket.org/pyrexed/ms/downloads).