import java.io.File;
import java.net.URL;

/** Created by roy on 4/4/16. */
class CourseResource {
    private final String fileName;
    private final URL url;
    private String subfolder = null;

    CourseResource(String fileName, URL url) {
        this.fileName = fileName;
        this.url = url;
        }

    void setSubfolder(String subfolder) {
        this.subfolder = subfolder;
    }

    URL getUrl() {
        return url;
    }

    String getFileName(boolean sf) {
        if (subfolder == null || !sf)
            return fileName;
        else {
            File file = new File(subfolder, fileName);
            return file.getPath();
        }
    }
    String getFileName(){
        return getFileName(true);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CourseResource that = (CourseResource) o;

        return url.equals(that.url);

    }

    @Override
    public int hashCode() {
        return url.hashCode();
    }
}
