/* Created by roy on 2/15/16. */

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Course implements Callable<String> {
    private final URL rootURL;
    private final String courseName;
    private final String courseNum;
    private final HttpsClient session;
    private final Set<CourseResource> resourceLinks;
    private String base_dir;
    Course(URL rootURL, String courseName, String courseNum, HttpsClient session)  {
        this.rootURL = rootURL;
        this.courseName = courseName;
        this.courseNum = courseNum;
        this.session = session;
        resourceLinks = new HashSet<>();

    }

    @Override
    public String call() throws Exception {
        processCourse();
        downloadCourse();
        return courseNum;
    }

    void setBase_dir(String base_dir){this.base_dir = base_dir;}

    String getCourseName() {
        return courseName;
    }

    private String resolveSubFolder(Element element){
        Element topic = element.parent().parent();
        for (Element p : topic.children()){
            if (p.hasClass("sectionname")) return p.getElementsByClass("sectionname").text();
        }
        return null;
    }

    private int processCourse() {
        try {
            print("Started processing course: %s", courseNum);
            String page_content = session.GetContent(rootURL.toString());
            Document doc = Jsoup.parse(page_content);
            // dealing with regular resources
            Elements resources = doc.getElementsByClass("resource");
            for (Element ele : resources) {
                if (Thread.currentThread().isInterrupted()) return 0;
                try {
                    Element temp_link = ele.select("a[href]").first();
                    URL link = new URL(temp_link.attr("href"));
                    CourseResource resource_full_link = resolveLink(link);
                    if (resource_full_link != null) {
                        String subFolder = resolveSubFolder(ele);
                        resource_full_link.setSubfolder(subFolder);
                        resourceLinks.add(resource_full_link);
                    }
                } catch (MalformedURLException ex) {
                    print("Error in parsing link");
                }
            }

            // dealing with assignments (homework)
            Elements assignments = doc.getElementsByClass("assign");
            List<CourseResource> assignments_links = new ArrayList<>();
            for (Element ele : assignments) {
                try {
                    Element temp_link = ele.select("a[href]").first();
                    URL link = new URL(temp_link.attr("href"));
                    assignments_links.addAll(resolveAssign(link));
                } catch (MalformedURLException ex) {
                    print("Error in parsing link");
                }
            }
            resourceLinks.addAll(assignments_links);

            // dealing with subfolders
            Elements subs = doc.getElementsByClass("folder");
            List<CourseResource> sub_links = new ArrayList<>();
            for (Element ele : subs) {
                try {
                    Element temp_link = ele.select("a[href]").first();
                    URL link = new URL(temp_link.attr("href"));
                    sub_links.addAll(resolveSub(link));
                } catch (MalformedURLException ex) {
                    print("Error in parsing link");
                }
            }
            resourceLinks.addAll(sub_links);
            return resourceLinks.size();
        } catch (Exception e) {
            print("error occurred at processCourses: %s" , e.toString());
            e.printStackTrace();
            return 0;
        }
    }

    private List<CourseResource> resolveAssign(URL assignment_link) {
        // Resolves a link to an assignments and extracts links to resources
        List<CourseResource> resolved = new ArrayList<>();
        try {
            Document doc = Jsoup.parse(session.GetContent(assignment_link.toString()));
            Elements hw_links = doc.getElementById("intro").select("a[href]");
            for (Element link : hw_links) {
                CourseResource res = resolveLink(new URL(link.attr("href")));
                resolved.add(res);
            }
        } catch (Exception e) {
            print("Failed to resolve assignment: " + assignment_link);
            print("With error: " + e.getMessage());
            e.printStackTrace();
        }
        return resolved;
    }

    private List<CourseResource> resolveSub(URL sub_link) {
        // Resolves a link to an sub-directories and extracts links to resources
        List<CourseResource> resolved = new ArrayList<>();
        try {
            Document doc = Jsoup.parse(session.GetContent(sub_link.toString()));
            Elements sub_links = doc.getElementsByClass("filemanager").select("a[href]");
            for (Element link : sub_links) {
                CourseResource res = resolveLink(new URL(link.attr("href")));
                resolved.add(res);
            }
        } catch (Exception e) {
            print("With error: " + e.getMessage());
            e.printStackTrace();
        }
        return resolved;
    }


    private CourseResource resolveLink(URL link) {
        try {
            URL full_link;
            String name = null;
            String html = session.GetContent(link.toString());
            Document doc = Jsoup.parse(html);
            // method 1: finding the link in the html source code
            Element temp = doc.getElementsByClass("resourceworkaround").select("a[href]").first();
            if (temp == null) {
                temp = doc.getElementsByClass("resourcecontent").select("a[href]").first();
            }
            if (temp != null) {
                full_link = new URL(temp.attr("href"));
                name = temp.childNode(0).toString();
            }
            // method 2: finding the link in the redirection
            else {
                full_link = session.getRedirect(link.toString());
                Pattern regex = Pattern.compile(".+/(.+?\\.[\\w\\d]{1,4})");
                Matcher regexMatcher = regex.matcher(full_link.toString());
                while (regexMatcher.find()) {
                    name = regexMatcher.group(1);
                }
            }
            return new CourseResource(name, full_link);
        } catch (Exception ex) {
            print("Error occurred in resolveLink: %s", link.toString());
            ex.printStackTrace();
            return null;
        }
    }

    private void downloadFromUrl(URL url, String localFilename) throws IOException {
        InputStream is = null;
        FileOutputStream fos = null;
        double countBytes = 0.0;

        try {
            URLConnection urlConn = url.openConnection();//connect

            is = urlConn.getInputStream();               //get connection inputstream
            fos = new FileOutputStream(URLDecoder.decode(localFilename, "UTF-8"));   //open outputstream to local file

            byte[] buffer = new byte[4096];              //declare 4KB buffer
            int len;

            //while we have available data, continue downloading and storing to local file
            while ((len = is.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
                countBytes += len;
            }
        } finally {
            try {
                if (is != null) is.close();
                if (fos != null) fos.close();
                ControlsPropagator.setDownload(countBytes);
            }
            catch (IOException e){
                print("Stream closing error when downloading: " + e.getMessage());
            }
        }
    }

    private Boolean checkCreateSubFolder(File file){
        String folderPath = file.getParent();
        try {
            File folder = new File(folderPath);
            if (!folder.exists() && folder.mkdirs())
                print("Created path for: " + folder.getAbsolutePath());
            return true;
        }
        catch (SecurityException e){
            if (folderPath != null) print("Insufficient permissions to create folder: " + folderPath);
            return false;
        }
        catch (NullPointerException e){
            print("Error in creating File object for folder while creating subfolders.");
            return false;
        }
    }

    private void downloadCourse() {
        try {
            File path = new File(base_dir, courseNum);
            if (!path.exists()) {
                if (path.mkdirs()) {
                    print("Created Directory: %s", path.getAbsolutePath());
                } else {
                    print("Failed to create directory: %s", path.getAbsolutePath());
                }
            }
            for (CourseResource resource : resourceLinks) {
                if (Thread.currentThread().isInterrupted()) return;
                File dfile = new File(path, resource.getFileName(ControlsPropagator.getSubFolder()));

                // if unable to create folder continue loop
                if (!checkCreateSubFolder(dfile)) continue;
                int retry_count = 1;
                while (dfile.exists()) {
                    String temp_name = dfile.getName();
                    String ext;
                    if (temp_name.lastIndexOf('.') > -1){
                        ext = temp_name.substring(temp_name.lastIndexOf('.')+1, temp_name.length());
                        temp_name = temp_name.substring(0,temp_name.lastIndexOf('.'));
                    } else {
                        ext = "";
                    }
                    String[] temp_array = temp_name.split("(_\\d)$");
                    dfile = new File(dfile.getParentFile().getAbsolutePath(), temp_array[0] + "_" + retry_count + "." + ext);
                    retry_count++;
                    if (retry_count > 10) {
                        print("Too many files with the same name: %s", temp_name);
                        break;
                    }
                }
                try {
                    downloadFromUrl(resource.getUrl(), dfile.getAbsolutePath());
//                    print("Downloaded file: %s", dfile.getName());
                } catch (IOException ex) {
                    print("Failed to download %s", resource.getFileName());
                    ex.printStackTrace();
                }
            }
        }
        catch (NullPointerException e){
            print("Null pointer exception in course downloader");
        }

    }
    private static void print(String msg, Object... args) {
        ControlsPropagator.setLogData(String.format(msg, args));
    }
}

