import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Stream;


/**Created by roy on 3/12/16.
 * Controls both views and orchestrates the main flow of the app.
 */
public class MSController extends Application {
    private static Stage primaryStage;
    private MoodleScraper scraper;
    private List<CheckBox> courseCheckBoxes;
    private ExecutorService executorService;
    private final List<ImageView> marks = new ArrayList<>();
    private final Image imageDone = new Image(getClass().getResourceAsStream("icon_check.gif"));
    @FXML private TextField username;
    @FXML private PasswordField pw;
    @FXML private Label status;
    @FXML private ProgressBar progress;
    @FXML private Button startB;
    @FXML private Button stopB;
    @FXML private Button stopBtn;
    @FXML private Button downloadB;
    @FXML private TextField pathF;
    @FXML private TextArea messages;
    @FXML private CheckBox subFolderCheck;
    @FXML private Label downloadC;
    private Task<MoodleScraper> connectTask;
    private final ContextMenu dummyContextMenu = new ContextMenu();


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        MSController.primaryStage = primaryStage;
        MSController.primaryStage.setTitle("MoodleScraper");
        initRootLayout();

    }

    private void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("ms.fxml"));
            Pane rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void buildCourseScene() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("courses.fxml"));
            loader.setController(this);

            //Flow pane for enumeration of available courses
            Pane courseLayout = loader.load();
            FlowPane flow = new FlowPane();
            flow.setPadding(new Insets(5.0, 50.0, 5.0, 50.0));
            flow.setVgap(10.0);
            flow.setHgap(25.0);
            flow.setPrefWrapLength(400.0); // preferred width allows for two columns
            flow.setPrefSize(200.0, 200.0);

            // Binds the text area for logging information.
            messages.textProperty().bindBidirectional(ControlsPropagator.logDataProperty());
            downloadC.textProperty().bind(ControlsPropagator.downloadProperty());
            //Configuring subfolder checkbox
            subFolderCheck.selectedProperty().bindBidirectional(ControlsPropagator.subFolderProperty());
            ControlsPropagator.subFolderProperty().setValue(true);

            // Populates the course list
            courseCheckBoxes = new ArrayList<>();
            for (String courseInfo : scraper.getCoursesNumbers()) {
                String courseName = scraper.getCourseName(courseInfo);
                HBox box = new HBox();
                CheckBox courseCheckBox = new CheckBox();
                Label courseLabel = new Label(URLDecoder.decode(courseName, "UTF-8"));
                courseCheckBox.setId(courseInfo);
                courseCheckBox.setSelected(true);
                ImageView graphicPlaceholder = new ImageView();
                graphicPlaceholder.setId(courseInfo);
                marks.add(graphicPlaceholder);
                box.getChildren().addAll(courseLabel, courseCheckBox, graphicPlaceholder);
                box.setSpacing(5.0);
                box.setMinWidth(320.0);
                box.setMaxWidth(320.0);
                box.setAlignment(Pos.CENTER_RIGHT);
                courseCheckBoxes.add(courseCheckBox);
                flow.getChildren().add(box);
            }
            flow.setAlignment(Pos.CENTER);
            courseLayout.getChildren().add(0, flow);
            StackPane.setAlignment(flow, Pos.CENTER);
            StackPane.setMargin(flow, new Insets(0.0, 0.0, 250.0, 0.0));
            pathF.setText(getCurrentPath());
            pathF.setContextMenu(dummyContextMenu);
            pathF.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    event.consume();
                    directorySelector();
                }
            });

            // Show the scene containing the root layout.
            Scene scene = new Scene(courseLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getCurrentPath() {
        return new File(MSController.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent();
    }


    @FXML
    public void connect() throws Exception {
        connectTask = new Task<MoodleScraper>() {
            @Override
            protected MoodleScraper call() throws Exception {
                stopB.setDisable(false);
                String usernameString = username.getText();
                String pwString = pw.getText();
                scraper = new MoodleScraper();
                Platform.runLater(() -> {
                    startB.setDisable(true);
                    status.setText("Logging in");
                    progress.setProgress(0.33);
                });
                scraper.createSession(usernameString, pwString);
                if (isCancelled()) {
                    return null;
                }
                Platform.runLater(() -> {
                    status.setText("Getting courses resources");
                    progress.setProgress(0.66);
                });
                Integer courseNum = scraper.getCourses();
                if (isCancelled()) return null;
                if (courseNum <= 0)
                    throw new LoginException("Wrong password.");

                Platform.runLater(() -> {
                    status.setText(String.format("Got %d courses", courseNum));
                    progress.setProgress(1.0);
                    startB.setDisable(false);
                });

                return scraper;
            }

        };
        connectTask.setOnSucceeded(t -> {
            scraper = connectTask.getValue();
            buildCourseScene();
        });
        connectTask.setOnCancelled(t -> {
            startB.setDisable(false);
            stopB.setDisable(true);
            status.setText("Cancelled");
            progress.setProgress(0.0);
        });
        connectTask.setOnFailed(t -> {
            startB.setDisable(false);
            stopB.setDisable(true);
            status.setText("Failed to login. Probably wrong credentials.");
            progress.setProgress(0.0);
        });
        Thread connectThread = new Thread(connectTask);

        connectThread.start();

    }

    @FXML
    public void downloadCourses() {
        Thread thread = new Thread(() -> {
            unmarkAll();
            stopBtn.setDisable(false);
            downloadB.setDisable(true);
            Stream<String> selectedCourses = courseCheckBoxes.stream().filter(CheckBox::isSelected).map(CheckBox::getId);
            executorService = Executors.newWorkStealingPool();
            List<Callable<String>> callables = new ArrayList<>();
            final String path;
            try {
                path = getPath();
                selectedCourses.forEach(p -> {
                    Course course = scraper.getCourse(p);
                    course.setBase_dir(path);
                    callables.add(course);
                });
                executorService.invokeAll(callables)
                        .stream()
                        .map(future -> {
                            try {
                                return future.get();
                            } catch (InterruptedException | ExecutionException | CancellationException e) {
                                e.printStackTrace();
                                return null;
                            }

                        }).forEach(p -> {
                            if (p != null) {
                                markDone(p);
                                print("Done with course: " + p);
                            }
                });
            } catch (InterruptedException e) {
                print("Fatal error: " + e.getMessage());
                e.printStackTrace();
            } catch (IOException e) {
                print("Failed to resolve JAR location");
                e.printStackTrace();
                return;
            }
            stopBtn.setDisable(true);
            downloadB.setDisable(false);
        });
        thread.start();
    }

    private void markDone(String courseNum) {
        marks.forEach(p -> {
            if (p.getId().equals(courseNum)) p.setImage(imageDone);
        });

    }

    private void unmarkAll() {
        marks.forEach(p -> p.setImage(null));

    }

    private String getPath() throws IOException {
        String path = pathF.getText().replace("\\", "\\\\");
        try {
            File file = new File(path);
            if (!file.isDirectory())
                if (!file.mkdir()) print("Can't create path: " + file.getAbsolutePath());
            if (file.exists()) return file.getAbsolutePath();
            else {
                if (file.mkdir()) return file.getAbsolutePath();
                else print("Can't find path: " + file.getAbsolutePath());
            }
        } catch (NullPointerException e) {
            print("Null Pointer on creating path: " + path);
        }
        return getCurrentPath();
    }

    private void print(String message) {
        ControlsPropagator.setLogData(message);
    }

    @FXML
    public void clearSelection() {
        courseCheckBoxes.forEach(p -> p.setSelected(false));
    }

    @FXML
    public void setSelection() {
        courseCheckBoxes.forEach(p -> p.setSelected(true));
    }

    @FXML
    private void stopApp() {
        stopBtn.setDisable(true);
        downloadB.setDisable(false);
        executorService.shutdownNow();
    }

    @FXML
    public void quitApp() {
        try {
            if (executorService != null && !executorService.isShutdown()) stopApp();
            primaryStage.close();
            stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void stopConnection() {
        connectTask.cancel();
    }

    private void directorySelector() {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("MoodleScraper");
        File defaultDirectory = new File(getCurrentPath());
        dirChooser.setInitialDirectory(defaultDirectory);
        File file = dirChooser.showDialog(primaryStage);
        if (file != null)
            pathF.setText(file.getAbsolutePath());
    }
}
